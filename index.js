//1
db.fruits.aggregate([

    {$match: {$and:[ {supplier:"Yellow Farms"}, {price:{$lt: 50}} ] }},
    {$count: "yellowFarmsLowerThan50"}

])

//2    
db.fruits.aggregate([

    {$match: {price:{$lt:30}}},
    {$count: "itemsPriceLessThan30"}

])

//3    
db.fruits.aggregate([
     
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"Yellow Farms", avgPrice:{$avg:"$price"}}}

])

//4    
db.fruits.aggregate([

       {$match:{supplier:"Red Farms Inc."}},
       {$group: {_id:"Red Farms Inc.", maxPrice:{$max:"$price"}}}

])

//5       
db.fruits.aggregate([

       {$match:{supplier:"Red Farms Inc."}},
       {$group: {_id:"Red Farms Inc.", minPrice:{$min:"$price"}}}

])     